<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="tyylit.css" type="text/css"/>
    <title>Tiedostopalvelin</title>
</head>
<body>
    <h3>Tiedostopalvelin</h3>
    <div>
      <a href="upload_lomake.php">Lisää tiedosto</a>
    </div>
    <table>
      <tr><th>Tiedoston nimi</th></tr>
      <?php
      $hakemisto="uploads";
      $osoitin=opendir($hakemisto);
      if ($osoitin) {
          while (false !== ($tiedosto=readdir($osoitin))) {
              $tiedostoPolunOsat=explode('.', $tiedosto);
              $paate=end($tiedostoPolunOsat);
              if (strcmp($paate, "pdf") == 0) {
                  $polku=$hakemisto . "/$tiedosto";
                  print "<tr>";
                  print "<td><a href='$polku'>$tiedosto</a></td>";
                  print "<tr>";
              }
          }
          closedir($osoitin);
      }   
      ?>
    </table>    
</body>
</html>
